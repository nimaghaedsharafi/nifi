<?php 

define("ADMIN_ACCESS", 0);
define("USER_ACCESS", 1);

define("STORY_UNPUBLISHED", 0);
define("STORY_PUBLISHED", 1);

define("COMMENT_DISABLED", 0);
define("COMMENT_ENABLED", 1);

define("COMMENT_NOTCHECKED", 0);
define("COMMENT_APPROVED", 1);
define("COMMENT_IGNORED", 2);

// Alert Messages Color
define('SUCCESS_ALERT', 'alert-success');
define('INFO_ALERT', 'alert-info');
define('WARNING_ALERT', 'alert-warning');
define('DANGER_ALERT', 'alert-danger');

?>