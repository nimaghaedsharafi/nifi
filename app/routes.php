<?php

require "pattern.php";
require "modelBind.php";

Route::get('/', ['as' => 'home', 'after' => 'incview', 'uses' => 'HomeController@showWelcome']);

Route::group(['prefix' => 'slack', 'after' => 'incview'], function () {
	Route::get('', ['as' => 'slackinv.new', 'uses' => 'SlackController@create']);
	Route::get('create', ['as' => 'slackinv.new', 'uses' => 'SlackController@create']);
	Route::post('store', [ 'before' => 'crsf' , 'as' => 'slackinv.new', 'uses' => 'SlackController@store' ]);
	Route::get('thankyou', ['as' => 'slackinv.thankyou', 'uses' => 'SlackController@thankyou']);	
});

Route::group(['prefix' => 'auth'], function () {
	Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin' ]);
	Route::post('login', [ 'before' => 'crsf' , 'as' => 'login', 'uses' => 'AuthController@postLogin' ]);
	Route::get('logout', ['before' => 'auth' , 'as' => 'logout', 'uses' => 'AuthController@logout' ]);
	Route::get('forgotpass', ['as' => 'forgotpass', 'uses' => '@' ]);
	
});

Route::group(['prefix' => 'story', 'after' => 'incview'], function () {
	Route::get('{storyId}/show', ['as' => 'story.show', 'uses' => 'StoryController@show' ]);
	Route::post('{storyId}/comment/leave', [ 'before' => 'crsf' , 'as' => 'story.comment.leave', 'uses' => 'CommentController@store' ]);
});

Route::group(['prefix' => 'backend', 'before' => 'adminAccess'], function() {
	
	Route::get('home', ['as' => 'bhome', 'uses' => 'BackendController@index' ]);
	
	Route::get('stat', ['as' => 'backend.stat', 'uses' => 'BackendController@stat' ]);
	
	Route::group(['prefix' => 'category'], function() {
		Route::get('index', ['as' => 'category.index', 'uses' => 'CategoryController@index' ]);
		Route::get('new', ['as' => 'category.new', 'uses' => 'CategoryController@create' ]);
		Route::post('new', [ 'before' => 'crsf' , 'as' => 'category.new', 'uses' => 'CategoryController@store' ]);
		Route::get('{catId}/delete', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy' ]);

	});
	Route::group(['prefix' => 'story'], function() {
	
		Route::get('new', ['as' => 'story.new', 'uses' => 'StoryController@create' ]);
		Route::post('new', [ 'before' => 'crsf' , 'as' => 'story.new', 'uses' => 'StoryController@store' ]);
		Route::get('index', ['as' => 'story.index', 'uses' => 'StoryController@index' ]);
		Route::get('{storyId}/delete', ['as' => 'story.destroy', 'uses' => 'StoryController@destroy' ]);
		Route::get('{storyId}/comments', ['as' => 'story.comment.index', 'uses' => 'CommentController@story' ]);
	});

	Route::group(['prefix' => 'comment'], function () {

		Route::get('index', ['as' => 'comment.index', 'uses' => 'CommentController@index' ]);
		Route::get('notchecked', ['as' => 'comment.notchecked', 'uses' => 'CommentController@notchecked' ]);
		Route::get('{commentId}/show', ['as' => 'comment.show', 'uses' => 'CommentController@show' ]);
		Route::get('{commentId}/approve', ['as' => 'comment.approve', 'uses' => 'CommentController@approve' ]);
		Route::get('{commentId}/ignore', ['as' => 'comment.ignore', 'uses' => 'CommentController@ignore' ]);
		Route::get('{commentId}/delete', ['as' => 'comment.destroy', 'uses' => 'CommentController@destroy' ]);
		Route::get('{commentId}/reply', ['as' => 'comment.get.reply', 'uses' => 'CommentController@getReply' ]);
		Route::post('{commentId}/reply', [ 'before' => 'crsf', 'as' => 'comment.reply', 'uses' => 'CommentController@reply' ]);
	});

	Route::group(['prefix' => 'slack'], function () {

		Route::get('index', ['as' => 'slack.index', 'uses' => 'SlackController@index' ]);
	});
	Route::group(['prefix' => 'config'], function () {

		Route::get('index', ['as' => 'config.index', 'uses' => 'BackendController@config' ]);
	});

	Route::get('mig', ['as' => 'mig', 'uses' => 'BackendController@migrate' ]);
	
});

