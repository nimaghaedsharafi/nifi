<?php 	
	Route::pattern('id', '[0-9]+');
	Route::pattern('token', '[0-9][a-zA-Z]+');
	Route::pattern('by', '[a-zA-Z_]+');
	Route::pattern('dir', 'asc|desc');
	Route::pattern('encodedID', '[0-9a-zA-Z]+');
	Route::pattern('uriadrs', '[0-9a-zA-Z_-]+');
?>