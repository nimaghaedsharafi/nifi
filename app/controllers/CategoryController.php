<?php

class CategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /category
	 *
	 * @return Response
	 */
	public function index()
	{
		$by = 'sort';
		$dir = 'asc';

		$dbData = Category::orderBy($by, $dir)->paginate();

		$data['title'] = 'لیست دسته بندی ها';

		$data['table'] = (new MyTable($this->indexTableConfig($dbData, $by, $dir)))->render();

		$data['links'] = $dbData->links();

		return View::make('category.index', $data);
	}

	public function indexTableConfig($dbData, $by, $dir) {

		$config['headers'] = $this->tableHeaderList();

		$config['cells'] = $this->tableCellList();

		$config['operation'] = $this->operationButtonList();

		$config['data'] = $dbData;
		
		$config['route'] = 'category.index';

		$config['onEmpty'] = 'هنوز دسته بندی ای تعریف نکرده اید.';

		$config['orderby'] = $by;

		$config['dir'] = $dir;

		return $config;
	}

	public function operationButtonList() {
		return [
			new ButtonItem('حذف', 'fa fa-trash-o', 'askConfirm', 'category.destroy', '$row->id'),
		];
	}
	public function tableCellList() {
		return [
			new Cell('name', '', ''),
			new Cell('sort', '', ''),
			new Cell('story()->count()', '', ''),
			new Cell('operation', '', ''),
		];
	}
	public function tableHeaderList() {
		return [
			'name' => new Header('نام', false),
			'sort' => new Header('اولویت', false),
			'storyNum' => new Header('تعداد یادداشت', false),
			'operation' => new Header('عملیات', false),
		];
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /category/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title'] = 'دسته بندی جدید';

		if(Request::ajax()) {
		
			return View::make('category.ajax_new', $data);
		} else {
		
			return View::make('category.new', $data);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /category
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$rules = [
			'name' => 'required'
		];
		$messages = [
			'name.required' => 'فیلد نام اجباری است.'
		];    

		$validation = Validator::make($input, $rules, $messages);

		if($validation->fails()) {
			return Redirect::route('category.new')->withInput()->withErrors($validation);
		}


		try {
			//make a new category
			$category = new Category;
			
			// prepare category to save
			$category->name = $input['name'];
			$category->sort = ($input['sort'] == '')? 0 : $input['sort'];

			// save the category
			$category->save();

		} catch (Exception $e) {

			return Redirect::route('category.new')->withInput()->withErrors($validation);
		}

//		DB::raw('update categories set `sort`=`sort` % (select count(*) from categories)');

		Session::flash('msg', 'دسته بندی '. Misc::makeMeBold($category->name) .' با موفقیت ساخته شد.');
		Session::flash('stat', SUCCESS_ALERT);

		return Redirect::route('category.index');
	}

	/**
	 * Display the specified resource.
	 * GET /category/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /category/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /category/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /category/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Category $category)
	{
		$name = $category->name;
		if($category->delete()) {
			Session::flash('msg', 'دسته بندی ' . Misc::makeMeBold($name) . ' با موفقیت حذف شد.');
			Session::flash('stat', SUCCESS_ALERT);
		} else {
			Session::flash('msg', 'در حذف دسته بندی ' . Misc::makeMeBold($name) . ' مشکلی پیش آمده است.');
			Session::flash('stat', DANGER_ALERT);
		}

		return Redirect::route('category.index');
	}

}