<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /comment
	 *
	 * @return Response
	 */
	public function index()
	{
		$dbData = Comment::orderby('id', 'desc')->paginate();	
		
		$data['title'] = 'مدیریت دیدگاه ها';
		
		$data['table'] = (new MyTable($this->indexTableConfig($dbData)))->render();

		$data['links'] = $dbData->links();
		
		return View::make('comment.index', $data);
	}

	public function notchecked() {
		
		$dbData = Comment::notChecked()->orderby('id', 'desc')->paginate();	
		
		$data['title'] = 'مدیریت دیدگاه های بررسی نشده';
		
		$data['table'] = (new MyTable($this->notcheckedTableConfig($dbData)))->render();

		$data['links'] = $dbData->links();
		
		return View::make('comment.notchecked', $data);
	}

	public function indexTableConfig($dbData) {

		$config['headers'] = $this->indexTableHeaderList();

		$config['cells'] = $this->indexTableCellList();

		$config['operation'] = $this->operationButtonList();

		$config['data'] = $dbData;
		
		$config['route'] = 'commnet.index';

		$config['onEmpty'] = 'هنوز دیدگاهی ثبت نشده است.';

		return $config;
	}

	public function notcheckedTableConfig($dbData) {

		$config['headers'] = $this->tableHeaderList();

		$config['cells'] = $this->tableCellList();

		$config['operation'] = $this->operationButtonList();

		$config['data'] = $dbData;
		
		$config['route'] = 'commnet.notchecked';

		$config['onEmpty'] = 'دیدگاه بررسی نشده ای وجود ندارد.';

		return $config;
	}

	public function operationButtonList() {
		return [
			new ButtonItem('نمایش', 'fa fa-search', '', 'comment.show', '$row->id', ['toggle' => "ajaxModal"]),
			new ButtonItem('پاسخ', 'fa fa-reply', '', 'comment.reply', '[$row->id]', ['toggle' => "ajaxModal"]),
			new ButtonItem('قبول', 'fa fa-check-square', 'askApprove', 'comment.approve', '$row->id'),
			new ButtonItem('رد', 'fa fa-ban', 'askBan', 'comment.ignore', '$row->id'),
			new ButtonItem('حذف', 'fa fa-trash-o', 'askConfirm', 'comment.destroy', '$row->id'),
		];
	}
	public function tableCellList() {
		return [
			new Cell('name', '', ''),
			new Cell('email', '', ''),
			new Cell('comment_text', 'Str::limit', '77'),
			new Cell('story->name', '', ''),
			new Cell('created_at', 'Misc::niceDateForge', 'j F y - H:i'),
			new Cell('operation', '', ''),
		];
	}
	public function tableHeaderList() {
		return [
			'name' => new Header('نام', false),
			'email' => new Header('ایمیل', false),
			'comment_text' => new Header('متن دیدگاه', false),
			'storyname' => new Header('بر یادداشت', false),
			'created_at' => new Header('تاریخ ثبت', false),
			'operation' => new Header('عملیات', false),
		];
	}
	public function indexTableCellList() {
		return [
			new Cell('name', '', ''),
			new Cell('email', '', ''),
			new Cell('comment_text', 'Str::limit', '77'),
			new Cell('story->name', '', ''),
			new Cell('status', 'Misc::threeStateIcon', ['green' => COMMENT_APPROVED, 'blue' => COMMENT_NOTCHECKED, 'red' =>COMMENT_IGNORED]),
			new Cell('created_at', 'Misc::niceDateForge', 'j F y - H:i'),
			new Cell('operation', '', ''),
		];
	}
	public function indexTableHeaderList() {
		return [
			'name' => new Header('نام', false),
			'email' => new Header('ایمیل', false),
			'comment_text' => new Header('متن دیدگاه', false),
			'storyname' => new Header('بر یادداشت', false),
			'status' => new Header('وضعیت', false),
			'created_at' => new Header('تاریخ ثبت', false),
			'operation' => new Header('عملیات', false),
		];
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /comment
	 *
	 * @return Response
	 */
	public function store(Story $story)
	{
		if($story->publish == STORY_UNPUBLISHED) {
			return Redirect::route('home');
		} else if($story->comment_status == COMMENT_DISABLED) {
			return Redirect::route('story.show', [$story->id]);
		}
		$input = Input::all();
		$rules = [
			'name' => ['required', 'min:4'],
			'email' => ['required', 'email'],
			'commenttext' => ['required', 'max:5000']
		];
		$messages = [
			'name.required' => 'فراموش کردی اسمت رو به من بگی!',
			'name.min' => 'اسمت خیلی کوتاه هست!',
			'email.required' => 'فراموش کردی ایمیلت رو به من بگی!',
			'email.email' => 'فکر کنم تو وارد کردن ایمیل اشتباه کردی!',
			'commenttext.required' => 'نمی خواهی دیدگاهت رو برام بنویسی؟!',
			'commenttext.max' => 'ممنون که انقد وقت گذاشتی و دیدگاه نوشتی ولی خیلی طولانی هست.'
		];
		$validation = Validator::make($input, $rules, $messages);
		
		if ($validation->fails()) {
			$redirect = Redirect::back()->withInput()->withErrors($validation);
			$redirect->setTargetUrl($redirect->getTargetUrl() . '#commentform');
			return $redirect;
		}

		$comment = new Comment;
		$comment->comment_text = $input['commenttext'];
		$comment->name = $input['name'];
		$comment->email = $input['email'];

		$flag = false; 

		if(Auth::check()) {
			$flag = true;
			$comment->name = Auth::User()->getName();
			$comment->email = Auth::User()->email;
			$comment->status = COMMENT_APPROVED;
		} else {
			$comment->status = COMMENT_NOTCHECKED;
		}
		
		try {
			
			$story->comment()->save($comment);
			
		} catch (Exception $e) {

			$redirect = Redirect::back()->withInput();
			$redirect->setTargetUrl($redirect->getTargetUrl() . '#commentform');
			return $redirect;
		}
		
		$redirect = Redirect::back();
		if($flag) {
			$redirect->setTargetUrl($redirect->getTargetUrl() . '#' . md5('comment' . $comment->id));	
		} else {
			Session::flash('msg', 'متشکر ، دیدگاه شما با موفقیت ثبت شد پس از تایید نمایش خواهد داده شد.');
			Session::flash('stat', SUCCESS_ALERT);
			
			$redirect->setTargetUrl($redirect->getTargetUrl() . '#msg');	
		}
		return $redirect;
	}

	/**
	 * Display the specified resource.
	 * GET /comment/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Comment $comment)
	{
		$data['comment'] = $comment;
		$data['title'] = 'نمایش';

		if(Request::ajax()) {
		
			return View::make('comment.ajax_show', $data);
		} else {
		
			return View::make('comment.show', $data);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /comment/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Comment $cm)
	{
		return ;
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /comment/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Comment $cm)
	{
		return ;
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /comment/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Comment $comment)
	{
		$name = $comment->name;
		$notename = $comment->story->name;

		if($comment->delete()) {
			Session::flash('msg', 'دیدگاه  ' . Misc::makeMeBold($name) . ' در یادداشت '. Misc::makeMeBold($notename) .' با موفقیت ' . Misc::makeMeBold('حذف') .' شد.');
			Session::flash('stat', SUCCESS_ALERT);
		} else {
			Session::flash('msg', 'در حذف دیدگاه ' . Misc::makeMeBold($name) . ' مشکلی پیش آمده است.');
			Session::flash('stat', DANGER_ALERT);
		}

		return Redirect::back();
	}

	public function ignore(Comment $comment)
	{
		$name = $comment->name;
		$notename = $comment->story->name;

		try {

			$comment->status = COMMENT_IGNORED;
			$comment->save();

			Session::flash('msg', 'دیدگاه  ' . Misc::makeMeBold($name) . ' در یادداشت '. Misc::makeMeBold($notename) .' با موفقیت ' . Misc::makeMeBold('رد') .' شد.');
			Session::flash('stat', SUCCESS_ALERT);
			
		} catch(Exception $e) {
			
			Session::flash('msg', 'در رد دیدگاه ' . Misc::makeMeBold($name) . ' مشکلی پیش آمده است.');
			Session::flash('stat', DANGER_ALERT);
		}

		return Redirect::back();
	}
	public function approve(Comment $comment)
	{
		$name = $comment->name;
		$notename = $comment->story->name;

		try {

			$comment->status = COMMENT_APPROVED;
			$comment->save();

			Session::flash('msg', 'دیدگاه  ' . Misc::makeMeBold($name) . ' در یادداشت '. Misc::makeMeBold($notename) .' با موفقیت ' . Misc::makeMeBold('تائید') .' شد.');
			Session::flash('stat', SUCCESS_ALERT);
			
		} catch(Exception $e) {
			
			Session::flash('msg', 'در تائید دیدگاه ' . Misc::makeMeBold($name) . ' مشکلی پیش آمده است.');
			Session::flash('stat', DANGER_ALERT);
		}

		return Redirect::back();
	}

	public function story(Story $story) {

		$dbData = $story->comment()->orderby('id', 'desc')->paginate();	
		
		$data['title'] = 'مدیریت دیدگاه های ' . $story->name;
		
		$data['table'] = (new MyTable($this->indexTableConfig($dbData)))->render();

		$data['links'] = $dbData->links();
		
		return View::make('comment.index', $data);
	}
	public function getReply(Comment $comment)
	{
		$data['comment'] = $comment;
		$data['title'] = 'پاسخ به دیدگاه';

		if(Request::ajax()) {
		
			return View::make('comment.ajax_reply', $data);
		} else {
		
			return View::make('comment.reply', $data);
		}
	}
	public function reply(Comment $comment)
	{
		$input = Input::all();
		$rules = [
			'commenttext' => ['required', 'max:5000']
		];
		$messages = [
			'commenttext.required' => 'نمی خواهی دیدگاهت رو برام بنویسی؟!',
			'commenttext.max' => 'ممنون که انقد وقت گذاشتی و دیدگاه نوشتی ولی خیلی طولانی هست.'
		];
		$validation = Validator::make($input, $rules, $messages);
		
		if ($validation->fails()) {
			$redirect = Redirect::back()->withInput()->withErrors($validation);
			return $redirect;
		}
		$newComment = new Comment;
		$newComment->comment_text = $input['commenttext'];
		$newComment->name = Auth::User()->getName();
		$newComment->email = Auth::User()->email;
		$newComment->status = COMMENT_APPROVED;
		
		try {

			$comment->story->comment()->save($newComment);
		} catch (Exception $e) {
die(var_dump($e));
			$redirect = Redirect::back()->withInput();
			$redirect->setTargetUrl($redirect->getTargetUrl() . '#commentform');
			return $redirect;
		}
		$link = route('story.show', $comment->story->id) . '#' . md5('comment' . $newComment->id);

		Mail::send('emails.commentreply', ['name' => $comment->name, 'story_title' => $comment->story->name, 'link' => $link], function($message) use($comment)
		{
		    $message->subject('پاسخ به دیدگاه شما');
		    $message->to($comment->email);
		});
	
		Session::flash('msg', 'پاسخ به دیدگاه ثبت شد.');
		Session::flash('stat', SUCCESS_ALERT);
		
		return Redirect::back();
	}
}