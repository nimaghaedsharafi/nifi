<?php

class SlackController extends \BaseController {

	private $invurl = "https://geektalks.slack.com/api/users.admin.invite?t=";
	private $token = "xoxp-6014626181-6014623121-6019458419-4a6e27";
	/**
	 * Display a listing of the resource.
	 * GET /slack
	 *
	 * @return Response
	 */
	public function index()
	{
		$by = 'sort';
		$dir = 'asc';

		$dbData = Slack::paginate();

		$data['title'] = 'دعوتنامه اسلک';

		$data['table'] = (new MyTable($this->indexTableConfig($dbData, $by, $dir)))->render();

		$data['links'] = $dbData->links();

		return View::make('config.index', $data);
	}

	public function indexTableConfig($dbData, $by, $dir) {

		$config['headers'] = $this->tableHeaderList();

		$config['cells'] = $this->tableCellList();

		$config['operation'] = $this->operationButtonList();

		$config['data'] = $dbData;
		
		$config['route'] = 'slack.index';

		$config['onEmpty'] = 'هنوز کسی درخواست دعوتنامه نداده است.';

		$config['orderby'] = $by;

		$config['dir'] = $dir;

		return $config;
	}

	public function operationButtonList() {
		return [			
		];
	}
	public function tableCellList() {
		return [
			new Cell('email', '', ''),
			new Cell('firstname', '', ''),			
			new Cell('lastname', '', ''),			
			new Cell('created_at', 'Misc::niceDateForge' ,'j F y - H:i'),	
			new Cell('operation', '', ''),
		];
	}
	public function tableHeaderList() {
		return [
			'email' => new Header('ایمیل', false),
			'firstname' => new Header('نام', false),
			'lastname' => new Header('نام خانوادگی', false),
			'created_at' => new Header('تاریخ ثبت', false),
			'operation' => new Header('عملیات', false),
		];
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /slack/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['title'] = 'گروه اسلک';

		return View::make('slack.invitaion', $data);
	}

	private function getInviteURL() {
		return $this->invurl . time();
	}
	/**
	 * Store a newly created resource in storage.
	 * POST /slack
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$input = Input::all();
		$rules = [
			'email' => 'required|email|unique:slackinv,email',
			'firstname' => 'required',
			'passcode' => 'required|in:na3,bitchplz,na3bp'
		];
		$messages = [
			'email.email' => 'آدرس ایمیلت رو درست تایپ نکردی!',
			'email.unique' => 'با این ایملیل آدرس قبلن ثبت نام کردی!',
			'email.required' => 'فراموش کردی ایمیلت رو وارد کنی!!',
			'firstname.required' => 'نمی تونیم تو گروه بی اسم صدات کنیم که!',
			'passcode.required' => 'بدون رمز دسترسی نمیشه عضو شد!',
			'passcode.in' => 'رمز دسترسی رو اشتباه وارد کردی!'
		];
		
		$valdation = Validator::make($input, $rules, $messages);

		if($valdation->fails()) {

			return Redirect::back()->withErrors($valdation)->withInput();
		}

		$slack = new Slack;
		$slack->email = trim($input['email']);
		$slack->firstname = trim($input['firstname']);
		$slack->lastname = trim($input['lastname']);

		
		$fields = array(
			'email' => urlencode($slack->email),
			'first_name' => urlencode($slack->firstname),
			'token' => $this->token,
			'set_active' => urlencode('true'),
			'_attempts' => '1'
		);

		if($slack->lastname != '') {
			$fields['last_name'] = urlencode($slack->lastname);
		}

		// url-ify the data for the POST
		$fields_string = '';

		foreach($fields as $key=>$value) {
			$fields_string = $fields_string . $key . '=' . $value . '&';
		}
		// remove extra & at the end
		rtrim($fields_string, '&');

		// open connection
		$ch = curl_init();

		// set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $this->getInviteURL());
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
 		// execute and return string 
		$replyRaw = curl_exec($ch);
		$reply = json_decode($replyRaw, true);

		// close connection	
		curl_close($ch);		
	
		if ($reply['ok'] == false) {
			return Redirect::back()->withInput();
				
		} else {
			try {
				$slack->save();
			} catch(Exception $e) {
				Session::flash('msg', 'مشکلی پیش آمده لطفن دوباره تلاش کنید.');
				Session::flash('stat', DANGER_ALERT);
				return Redirect::back()->withInput();
			}
			Mail::send('slack.thankyou_email', $input, function($message) use ($input) {
			    $message->to($input['email'], $input['firstname'] . ' ' . $input['lastname'])->subject('عضویت شما به گروه اسلک');
			});
			Mail::send('slack.ccemail', $input, function($message) use ($input) {
				$message->to('ghaedsharafi.nima@gmail.com', $input['firstname'] . ' ' . $input['lastname'])->subject('عضو جدید اسلک');
			});
			Cache::put('slackemail', $input['email'], 60);
			Cache::put('slackname', $input['firstname'] . ' ' . $input['lastname'], 60);
			return Redirect::route('slackinv.thankyou');
			
		}
	}

	public function thankyou() {

		$data['title'] = 'گروه اسلک';
		$data['email'] = Cache::get('slackemail', 'abc@null.io');
		$data['name'] = Cache::get('slackname', 'John Doe');
		return View::make('slack.thankyou', $data);
	}

	/**
	 * Display the specified resource.
	 * GET /slack/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /slack/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /slack/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /slack/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}