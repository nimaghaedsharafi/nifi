<?php

class BackendController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /backend
	 *
	 * @return Response
	 */
	public function index()
	{
		return Redirect::route('story.index');
	}

	public function config() {	
		
		$by = 'sort';
		$dir = 'asc';

		$dbData = SConfig::paginate();

		$data['title'] = 'تنظیمات';

		$data['table'] = (new MyTable($this->indexTableConfig($dbData, $by, $dir)))->render();

		$data['links'] = $dbData->links();

		return View::make('config.index', $data);
	}

	public function indexTableConfig($dbData, $by, $dir) {

		$config['headers'] = $this->tableHeaderList();

		$config['cells'] = $this->tableCellList();

		$config['operation'] = $this->operationButtonList();

		$config['data'] = $dbData;
		
		$config['route'] = 'config.index';

		$config['onEmpty'] = 'تنظیماتاتی وجود ندارد!.';

		$config['orderby'] = $by;

		$config['dir'] = $dir;

		return $config;
	}

	public function operationButtonList() {
		return [			
		];
	}
	public function tableCellList() {
		return [
			new Cell('ckey', '', ''),
			new Cell('cval', '', ''),			
			new Cell('operation', '', ''),
		];
	}
	public function tableHeaderList() {
		return [
			'ckey' => new Header('نام', false),
			'cval' => new Header('مقدار', false),
			'operation' => new Header('عملیات', false),
		];
	}

	public function migrate() {
		return Artisan::call('migrate', ['--force' => true]);
	}

}