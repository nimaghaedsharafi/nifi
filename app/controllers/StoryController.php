<?php

class StoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /story
	 *
	 * @return Response
	 */
	public function index() {
		$by = 'id';
		$dir = 'desc';

		$dbData = Story::orderBy($by, $dir)->paginate();

		$data['title'] = 'لیست یادداشت ها';

		$data['table'] = (new MyTable($this->indexTableConfig($dbData, $by, $dir)))->render();

		$data['links'] = $dbData->links();

		return View::make('story.index', $data);
	}

	public function indexTableConfig($dbData, $by, $dir) {

		$config['headers'] = $this->tableHeaderList();

		$config['cells'] = $this->tableCellList();

		$config['operation'] = $this->operationButtonList();

		$config['data'] = $dbData;
		
		$config['route'] = 'story.index';

		$config['onEmpty'] = 'هنوز یادداشتی ننوشتید.';

		$config['orderby'] = $by;

		$config['dir'] = $dir;

		return $config;
	}

	public function operationButtonList() {
		return [
			new ButtonItem('دیدگاه ها', 'fa fa-comments', '', 'story.comment.index', '$row->id'),
			new ButtonItem('حذف', 'fa fa-trash-o', 'askConfirm', 'story.destroy', '$row->id'),
		];
	}
	public function tableCellList() {
		return [
			new Cell('name', '', ''),
			new Cell('category->name', '', ''),
			new Cell('publish', 'Misc::makeBooleanSign', ''),
			new Cell('comment_status', 'Misc::makeBooleanSign', ''),
			new Cell('view', '', ''),
			new Cell('comment->count()', '', ''),
			new Cell('created_at', 'Misc::niceDateForge', 'j F y - H:i'),
			new Cell('operation', '', ''),
		];
	}
	public function tableHeaderList() {
		return [
			'name' => new Header('موضوع', false),
			'catid' => new Header('دسته بندی', false),
			'publish' => new Header('وضعیت انتشار', false),
			'comment_status' => new Header('امکان دیدگاه', false),
			'view' => new Header('نمایش', false),
			'commentcount' => new Header('دیدگاه', false),
			'created_at' => new Header('تاریخ ثبت', false),
			'operation' => new Header('عملیات', false)
		];
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /story/create
	 *
	 * @return Response
	 */
	public function create() {
		$data['title'] = 'یادداشت جدید';
		$data['categories'] = Category::all()->lists('name', 'id');

		if(Request::ajax()) {
		
			return View::make('story.ajax_new', $data);
		} else {

			return View::make('story.new', $data);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /story
	 *
	 * @return Response
	 */
	public function store() {
		$input = Input::all();
		$rules = [
			'title' => 'required|max:200',
			'catid' => 'required|exists:categories,id',
			'shorttext' => 'required|max:21844',
			'fulltext' => 'required',
		];
		$messages = [
			'title.required' =>  'این فیلد الزامی است.',
			'title.max' =>  'حداکثر تعداد حروف این فیلد 200 کارکتر است.',
			'catid.required' => 'دسته بندی الزامی است.',
			'catid.exists' =>  'دسته بندی نامعتبر است.',
			'shorttext.required' => 'متن کوتاه یادداشت الزامی است.',
			'shorttext.max' => 'متن کوتاه خیلی طولانی است.',
			'fulltext.required' => 'متن کامل الزامی است.',
		];

		$valdation = Validator::make($input, $rules, $messages);

		if ($valdation->fails()) {

			return Redirect::route('story.new')->withInput()->withErrors($valdation);
		}

		try {
			// make a new story
			$story = new Story;

			// prepare story
			$story->name = $input['title'];
			$story->shorttext = $input['shorttext'];
			$story->fulltext = $input['fulltext'];
			$story->publish = !!!isset($input['status']);
			
			// exsitance check in validation!
			$story->catid = $input['catid'];

			Auth::User()->story()->save($story);

		} catch (Exception $e) {
			throw $e;
			
			return Redirect::route('story.new')->withInput()->withErrors($valdation);			
		}

		// make a message
		Session::flash('msg', 'یادداشت ' . Misc::makeMeBold($story->name) . ' با موفقیت ثبت شد.');
		Session::flash('stat', SUCCESS_ALERT);
		
		// Redirect user to story list
		return Redirect::route('story.index');
	}

	/**
	 * Display the specified resource.
	 * GET /story/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Story $story) {
		if($story->publish == STORY_UNPUBLISHED) {
			return Redirect::route('home');
		}

		$story->view = $story->view + 1;
		$story->save();
		 
		$data['story'] = $story;
		$data['comments'] = $story->comment()->approved()->orderby('id', 'desc')->get();

		return View::make('story.show', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /story/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /story/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /story/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Story $story) {
		$name = $story->name;

		if($story->delete()) {
			Session::flash('msg', 'یادداشت  ' . Misc::makeMeBold($name) . '  با موفقیت حذف شد.');
			Session::flash('stat', SUCCESS_ALERT);
		} else {
			Session::flash('msg', 'در حذف یادداشت ' . Misc::makeMeBold($name) . ' مشکلی پیش آمده است.');
			Session::flash('stat', DANGER_ALERT);
		}

		return Redirect::route('story.index');
	}
}