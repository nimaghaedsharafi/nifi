<?php

class AuthController extends \BaseController {

	public function getLogin() {
		
		// user already authenticated so he/she should not be here anymore
		if(Auth::check()) {
			return Redirect::route('bhome');
		}

		$data['date'] = jDate::forge()->format('%d %B %Y');

		if(Request::ajax()) {
			return View::make('home.ajax-login', $data);
		} 
		return View::make('home.login', $data);
	}


	public function postLogin() {
		
		$input = Input::all();
		
		$rules = [ 'username' => 'required', 'password' => 'required'];
		$validator = Validator::make($input, $rules);
		
		// if user sent us an ajax request so we do same
		if($validator->fails()){
			return Redirect::route('login')->with('msg', 'لطفاً تمام فیلد ها را پر کنید.')->with('stat', DANGER_ALERT);
		}

		// make an array to prepare everything for laravel attempt func.
		$credential = [
			'username' => $input['username'], 
			'password' => $input['password']
		];
		if(Auth::attempt($credential, !!isset($input['remember']))) {
			
			$user = Auth::getUser();
			// rehash user's password if it needs
			if(Hash::needsRehash($user->password)) {
				$user->password = Hash::make($credential['password']);
				$user->save();
			}
			// redirect user to intendede page or by default to homepage
			return Redirect::intended();
		}
		return Redirect::route('login')->with('msg', 'اطلاعات وارد شده صحیح نمی باشد.')->with('stat', DANGER_ALERT);
	}
	public function logout() {
		
		// log user out
		Auth::logout();

		return Redirect::route('home');
	}
}