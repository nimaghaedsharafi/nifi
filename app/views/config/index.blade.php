@extends('layouts.private.master')

@section('title')
	{{ $title }} 
@stop

@section('content')
	
	{{ $table }}

	<div class="pagination">
		<ul class="pagination">
			{{ $links }}
		</ul>
	</div>
@stop