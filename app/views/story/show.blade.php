@extends('layouts.public.master')

@section('title')
	{{ $story->name }}
@stop

@section('content')
	@include('layouts.public.article.fullshow', $story)

	@if($story->comment_status == COMMENT_ENABLED)
		<div class="comment">	
			<h3>دیدگاه های شما</h3>

			@include('story.comment.leave', [$story, $errors])

			@include('story.comment.show', $comments)
		</div>
	@endif
@stop

@section('script')
	{{ HTML::script('js/comment.js') }}
	<script type="text/javascript">
		$('.panel-body').highlight(/\B#\w+/);
	</script>

@stop