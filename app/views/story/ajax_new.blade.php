@extends('layouts.private.ajaxModal')

@section('title')
	{{ $title }} 
@stop

@section('formopen')
	{{ Form::open(array('route' => 'story.new')) }}
@stop

@section('content')
	{{ HTML::script('ckeditor/ckeditor.js'); }}
	<div class="row">
		<div class="col-md-5 col-sm-7 form-group">
			<label for="title"><span class="req"></span>موضوع مطلب</label>
			<input type="text" class="form-control" name="title" placeholder="موضوع مطلب" value="{{ Input::old('title', '') }}">
			{{ Misc::formError($errors, 'title') }}
		</div>
	</div>
	<div class="row">
		<div class="col-md-5 col-sm-6 form-group">
			<label for="catid"><span class="req"></span>دسته بندی</label>
			{{ Form::select('catid', $categories, Input::old('catid', 0), array('class' => 'form-control')) }}
			{{ Misc::formError($errors, 'catid') }}
			<span class="help-block">این یادداشت به کدام <strong>دسته</strong> تعلق دارد؟</span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-7 col-sm-12 form-group">
			<label for="shorttext"><span class="req"></span>متن کوتاه</label>
			{{ Form::textarea('shorttext', Input::old('shorttext', ''), array('id' => "shorttext", 'class' => 'form-control', 'rows' => 3)) }}

			{{ Misc::formError($errors, 'shorttext') }}
			<span class="help-block">قسمتی از <strong>یادداشت</strong> که در صفحه اصلی نمایش داده خواهد شد را اینجا وارد کنید.</span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 form-group">
			<label for="fulltext"><span class="req"></span>متن کامل</label>
			{{ Form::textarea('fulltext', Input::old('fulltext', ''), array('id' => "fulltext", 'class' => 'form-control', 'placeholder' => "متن کامل مطلب را اینجا وارد نمائید.")) }}
			{{ Misc::formError($errors, 'fulltext') }}
			<span class="help-block">قسمتی که در صفحه اصلی نمایش داده خواهد شد را اینجا وارد کنید.</span>
		</div>
	</div>
	<div class="checkbox form-group">
		<label>
			{{ Form::checkbox('status', 'true', Input::old('status', false)); }} ثبت موقت و عدم انتشار مطلب
			<span class="help-block">در صورت فعال سازی این گزینه ، مطلب منتشر نخواهد شد.</span>
		</label>
	</div>
@stop

@section('script')
	<script type="text/javascript">
		CKEDITOR.replace( 'fulltext', {
			language: 'fa'
		});
		CKEDITOR.replace( 'shorttext', {
			language: 'fa'
		});
	</script>
@stop

@section('footer')
	<input type="submit" value="ثبت" class="btn btn-success">
@stop