<div id="commentform">
	{{ Form::open(['route' => ['story.comment.leave', $story->id]]) }}
	 <div id="msg">{{ Misc::makeMessage() }} {{ Misc::allFormErrors($errors) }}</div>	
	 <div class="row">
		<div class="col-md-12 col-sm-12 form-group">
			{{ Form::textarea('commenttext', '',  ['id' => 'msgt', 'class' => 'form-control', 'placeholder' => 'دیدگاه خودتون رو اینجا بنویسید ...', 'rows' => 4]) }}
		</div>
	</div>
	 <div id="authComment" class="alert alert-warning" style="display:none">
	 <h4>لطفا خودتون رو معرفی کنید</h4>
	 <div id="error"></div>
		<div class="row">
			<div class="col-md-5 col-sm-5 form-group">
				<label for="name"><span class="req"></span>اسمتون: </label>
				<input type="text" id="name" class="form-control" name="name" placeholder="چی می تونم صداتون بزنم؟" value="{{ Input::old('name', '') }}">
			</div>
			<div class="col-md-7 col-sm-7 form-group">
				<label for="email"><span class="req"></span>آدرس ایمیل:</label>
				{{ Form::text('email', '',  array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'پیش من پنهون می مونه')) }}
			</div>
		</div>
		<p>
			<span>فقط اسمتون رو نمایش می دم. آدرس ایمیلتون فقط برای جلوگیری از اسپم هست.</span><br>
			<span>عکس شما از طریق <a href="http://gravatar.com/" target="_blank">Gravatar </a> شناسایی می شه.</span>
		</p>
	</div>
	<input type="submit" id="leavecomment" value="ارسال دیدگاه" class="btn btn-success">
	
	{{ Form::close() }}
</div>