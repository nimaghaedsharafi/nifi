<div class="comments">
	@foreach ($comments as $comment)
		@include('story.comment.show_individual', $comment)
	@endforeach
</div>