<div class="row" id="{{ md5("comment" . $comment->id) }}">
	<div class="col-sm-2 col-xs-2 namesec">
		<div class="thumbnail">
		<img class="img-responsive user-photo" src="{{ $comment->getGravatarURL() }}">
		<p class="text-center"><strong>{{{ $comment->name }}}</strong></p>
		</div><!-- /thumbnail -->
	</div><!-- /col-sm-1 -->

	<div class="col-sm-10 col-xs-10 panelbox">
		<div class="panel panel-default">
			<div class="panel-heading">
			<span class="text-muted">{{ Misc::NiceDateForge($comment->created_at) }}</span>
			</div>
			<div class="panel-body">{{{ $comment->comment_text }}}</div><!-- /panel-body -->
		</div><!-- /panel panel-default -->
	</div>
</div>