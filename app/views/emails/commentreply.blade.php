<!DOCTYPE html>
<html>
<body style="direction:rtl;unicode-bidi:embed;" style="font-size:12px">
<div>
    <h1 style="font-family:tahoma !important; font-size:16px">سلام {{ $name }}</h1>
    <p style="font-family:tahoma !important">
        به دیدگاه شما در یادداشت "{{ $story_title }}" پاسخ داده شده است برای خواندن پاسخ <a href="{{ $link }}">اینجا</a> را کلیک کنید و یا ادرس زیر را در مرورگر اینترنت خودتون وارد کنید. <br /><a href="{{ $link }}">{{ $link }}</a><br/>
        <h4 style="font-family:tahoma !important; font-size:13px">نیما قائدشرفی</h4>
    </p>
</div>
</body>
</html>