<article id="post-{{ $story->id }}" class="post">
	<h2><a href="{{ URL::route('story.show', $story->id) }}">{{ $story->name }}</a></h2>
	<h4>{{ Misc::niceDateForge($story->created_at, 'l j F y') }}</h4>
	<section class="content">
	<p>{{ $story->shorttext }}</p>
	<p id="continue">{{ $story->fulltext }}</p>
	</section>
</article>