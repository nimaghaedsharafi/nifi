<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="{{{ SConfig::get('person.name') }}} {{{ SConfig::get('person.about') }}}">
		<meta name="author" content="{{{ SConfig::get('person.name') }}}">
	<title>{{{ SConfig::get('system.name') }}} - @yield('title')</title>

		
		<!-- Loading Bootstrap -->    
		{{ HTML::style("bootstrap/css/bootstrap.min.css") }}
		<!-- fontawesome styles  -->
		{{ HTML::style("font-awesome/css/font-awesome.min.css") }}

		<!-- Custom styles for this template -->
		{{ HTML::style("css/public.css") }}

		@yield('style')

	</head>
	

<body>
	<!-- Main jumbotron for a primary marketing message or call to action -->
	@include('layouts.public.partial.header')

		<div class="container">
			<!-- Example row of columns -->
			<div class="row">
				<div class="col-md-10 col-xs-10 col-md-offset-1 col-xs-offset-1">
					@yield('content')
				</div>
			</div>

			<!-- Site footer -->
			@include('layouts.partial.footer')	

		</div> <!-- /container -->

	</body>
	
	{{ HTML::script("js/jquery-2.0.3.min.js") }}
	{{ HTML::script("bootstrap/js/bootstrap.min.js") }}
	{{ HTML::script("js/bootbox.min.js") }}
	{{ HTML::script('js/modalcreator.js') }}
	@yield('script')
</html>
