<div class="jumbotron whoiam text-center">
	<a href="{{ URL::route('home') }}"><h1 id="personname">{{{ SConfig::get('person.name') }}}</h1></a>
	<p id="personabout">{{{ SConfig::get('person.about') }}}</p>
	<p class="social">
		<a href="{{{ SConfig::get('person.bitbucket') }}}" target="_blank"><i class="fa fa-bitbucket fa-lg"></i>&nbsp;&nbsp;</a>
		<a href="{{{ SConfig::get('person.github') }}}" target="_blank"><i class="fa fa-github fa-lg"></i>&nbsp;&nbsp;</a>
		<a href="{{{ SConfig::get('person.facebook') }}}" target="_blank"><i class="fa fa-facebook fa-lg"></i>&nbsp;&nbsp;</a>
		<a href="{{{ SConfig::get('person.linkedin') }}}" target="_blank"><i class="fa fa-linkedin fa-lg"></i>&nbsp;&nbsp;</a>
		<a href="{{{ SConfig::get('person.twitter') }}}" target="_blank"><i class="fa fa-twitter fa-lg"></i>&nbsp;&nbsp;</a>
	</p>
</div>