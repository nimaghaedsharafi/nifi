<!-- Example row of columns <-->
@foreach($totalNews->chunk(3) as $news)
<div class="row">
	@foreach($news as $row)
	<div class="col-lg-4">
		<h4>{{ $row->title }}</h4>
		<p>{{ $row->short_text }}</p>
		<p><a target="_blank" class="btn btn-primary pull-right" href="{{ URL::route('news.show', $row->prefable_adrs) }}" role="button">ادامه &raquo;</a></p>
	</div>
	@endforeach
</div>
@endforeach