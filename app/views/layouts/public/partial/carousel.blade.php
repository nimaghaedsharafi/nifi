<?php if($sliders->count() != 0) { $first = true; ?>
<div class="jumbotron">
	<div class="row">
		<!-- The carousel -->
		<div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				
				<?php for($i = 0; $i < $sliders->count(); $i++) { ?>
					<?php if($i == 0) { ?>
						<li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
					<?php } else { ?>
						<li data-target="#transition-timer-carousel" data-slide-to="{{ $i }}"></li>
					<?php } ?>
				<?php } ?>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<?php foreach($sliders as $slider) { ?>
					<?php if($first) { $first = false; ?>
						<div class="item active">
					<?php } else { ?>
						<div class="item">
					<?php } ?>
					{{ HTML::decode(HTML::entities(HTML::link($slider->link, HTML::image($slider->picture, $slider->desc, ['title' =>  $slider->desc]), ['target' => '_blank']))) }}
				</div>
				<?php } ?>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#transition-timer-carousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#transition-timer-carousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div>
	</div>
</div>
<?php } ?>