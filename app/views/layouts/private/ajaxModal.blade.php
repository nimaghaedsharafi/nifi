@yield('style')
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="inTitle"><h4 class="modal-title">@yield('title')@yield('inTitle')</h4></div>
		</div>
		@yield('formopen')
			<div class="modal-body">
				@yield('content')
			</div>
			<div class="modal-footer">
				@yield('footer')
				&nbsp; یا <a href="#" class="btn" data-dismiss="modal">بستن</a>
			</div>
		</form>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
@yield('script')