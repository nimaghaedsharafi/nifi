<div class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
		<li class="nav-header">منو اصلی</li>
		<li><a href="{{ URL::route('home') }}" target="_blank"><i class="fa fa-home"></i> صفحه اصلی</a></li>
		<li><a href="{{ URL::route('config.index') }}"><i class="fa fa-gear"></i> تنظیمات سایت</a></li>
		<li><a href="{{ URL::route('logout') }}"><i class="fa fa-power-off"></i> خروج</a></li>
	</ul>
	<ul class="nav nav-sidebar">
		<li class="nav-header">یادداشت</li>
		<li><a href="{{ URL::route('story.new') }}"><i class="fa fa-pencil"></i> درج یادداشت جدید</a></li>
		<li><a href="{{ URL::route('story.index') }}"><i class="fa fa-newspaper-o"></i> مدیریت یادداشت ها</a></li>
	</ul>
	<ul class="nav nav-sidebar">
		<li class="nav-header">دسته بندی</li>
		<li><a data-toggle="ajaxModal" href="{{ URL::route('category.new') }}"><i class="fa fa-plus-square"></i> دسته بندی جدید</a></li>
		<li><a href="{{ URL::route('category.index') }}"><i class="fa fa-bars"></i> مدیریت دسته بندی ها</a></li>
	</ul>
	<ul class="nav nav-sidebar">
		<li class="nav-header">دیدگاه ها</li>
		<li><a href="{{ URL::route('comment.notchecked') }}"><i class="fa fa-plus-square"></i> دیدگاه های چک نشده</a></li>
		<li><a href="{{ URL::route('comment.index') }}"><i class="fa fa-bars"></i> مدیریت دیدگاه ها</a></li>
	</ul>
	<ul class="nav nav-sidebar">
		<li class="nav-header">اسلک</li>
		<li><a href="{{ URL::route('slack.index') }}"><i class="fa fa-plus-square"></i> دعوتنامه اسلک</a></li>
	</ul>
</div>