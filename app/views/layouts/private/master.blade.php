<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>@yield('title') - {{ SConfig::get('system.name') }}</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Loading Bootstrap -->    
		{{ HTML::style("bootstrap/css/bootstrap.min.css") }}
		{{ HTML::style('css/dashboard.css') }}	
		<!-- fontawesome styles  -->
		{{ HTML::style("font-awesome/css/font-awesome.min.css") }}
		{{ HTML::style("css/style.css") }}

		<link rel="shortcut icon" href="images/favicon.ico">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
		<!--[if lt IE 9]>
			{{ HTML::script("js/html5shiv.js") }}
			{{ HTML::script("js/respond.min.js") }}
		<![endif]-->
		@yield('style')
	</head>
	<body>		
		<!-- include('layouts.public.partial.menu2') -->

		<div class="container">
	      	<div class="row">
				@include('layouts.private.menu')
				<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
					<div class="inTitle"><h1 class="sub-header">@yield('title')@yield('inTitle')</h1></div>
					@yield('breadcrumbs')
					@yield('content')
				</div>
				<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
					@include('layouts.partial.footer')
				</div>
			</div>
		</div>
		<div class="modal-dialog" id="ajaxLoading" style="display:none">
			<div class="modal-content">
				<p>
					<div><i class="fa fa-3x fa-spinner"></i></div>
					<h4>لطفاً کمی تأمل فرمائید.</h4>
				</p>
			</div>
		</div>

		<!-- Load JS here for greater good =============================-->
		{{ HTML::script("js/jquery-2.0.3.min.js") }}
		{{ HTML::script("bootstrap/js/bootstrap.min.js") }}
		{{ HTML::script("js/bootbox.min.js") }}
		{{ HTML::script('js/modalcreator.js') }}
		{{ HTML::script('js/jquery.centerize.js') }}
		<script type="text/javascript">
			$( document ).ajaxStart(function() {
				$("#ajaxLoading").show().center();
			});
			$( document ).ajaxStop(function(){
				$("#ajaxLoading").hide();
			});
		</script>
		@yield('script')
	</body>
</html>