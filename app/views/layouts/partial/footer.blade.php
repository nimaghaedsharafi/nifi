<div class="footer copyright">
	<p>ساخته شده با <a href="http://laravel.com" target="_blank">لراول4</a> ، <a href="http://getbootstrap.com" target="_blank">بوت استرپ</a> و چند لیوان قهوه</p>
	<p>مطالب تحت لیسانس <a href="http://creativecommons.org/licenses/by-nc-nd/3.0/" rel="license" target="_blank">کریتیو کامنز</a> منتشر می‌شوند. ۱۳۹۳ - {{ jDate::forge()->reforge('+ 1 year')->format('Y'); }}</p>
</div>