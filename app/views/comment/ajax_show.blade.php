@extends('layouts.private.ajaxModal')

@section('title')
	{{ $title }} 
@stop

@section('content')
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<p>{{ $comment->comment_text }}</p>
		</div>
	</div>
@stop

@section('footer')
	@if ($comment->status == COMMENT_NOTCHECKED) 
		<a class="btn btn-success" href="{{ URL::route('comment.approve', [$comment->id]) }}">قبول</a>
		<a class="btn btn-warning" href="{{ URL::route('comment.ignore', [$comment->id]) }}">رد</a>
	@elseif ($comment->status == COMMENT_APPROVED)
		<a class="btn btn-warning" href="{{ URL::route('comment.ignore', [$comment->id]) }}">رد</a>
	@elseif ($comment->status == COMMENT_IGNORED)
		<a class="btn btn-success" href="{{ URL::route('comment.approve', [$comment->id]) }}">قبول</a>
	@endif
	<a class="btn btn-danger" href="{{ URL::route('comment.destroy', [$comment->id]) }}">حذف</a>
@stop