@extends('layouts.private.ajaxModal')

@section('title')
	{{ $title }} 
@stop
@section('formopen')
	{{ Form::open(array('route' => ['comment.reply', $comment->id])) }}
@stop
@section('content')
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<p>{{ $comment->comment_text }}</p>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 form-group">
				<label for="commenttext"><span class="req"></span>پاسخ</label>
				<textarea class="form-control" name="commenttext" row="5" col="10" placeholder="پاسخ خود را اینجا بنویسید" value="{{ Input::old('commenttext', '') }}" />
				{{ Misc::formError($errors, 'commenttext') }}
			</div>
		</div>
	</div>
@stop

@section('footer')
	<input class="btn btn-primary" type="submit" value="ثبت پاسخ" />
@stop