@extends('layouts.private.master')

@section('title')
	{{ $title }} 
@stop

@section('content')
	{{ Form::open(array('route' => 'comment.reply')) }}	
		<div class="row">
			<div class="col-md-7 col-xs-12">
				<p>{{ $comment->comment_text }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-sm-8 form-group">
				<label for="commenttext"><span class="req"></span>پاسخ</label>
				<textarea class="form-control" name="commenttext" row="5" col="10" placeholder="پاسخ خود را اینجا بنویسید" value="{{ Input::old('commenttext', '') }}"></textarea>
				{{ Misc::formError($errors, 'commenttext') }}
			</div>
		</div>
	<input type="submit" value="ثبت" class="btn btn-success">
	{{ Form::close() }}
@stop