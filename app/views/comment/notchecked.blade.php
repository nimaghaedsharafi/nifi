@extends('layouts.private.master')

@section('title')
	{{ $title }} 
@stop

@section('content')
	
	{{ Misc::makeMessage(); }}

	{{ $table }}

	<div class="pagination">
		<ul class="pagination">
			{{ $links }}
		</ul>
	</div>
@stop


@section('script')
	{{ HTML::script('js/askconfirm.js'); }}
	<script type="text/javascript">
		$('.askConfirm').click(function (e){
			askConfirm('آیا از حذف این دیدگاه اطمینان دارید؟', $(this), e);
		});
		$('.askApprove').click(function (e){
			askConfirm('آیا از تائید این دیدگاه اطمینان دارید؟', $(this), e);
		});
		$('.askBan').click(function (e){
			askConfirm('آیا از رد این دیدگاه اطمینان دارید؟', $(this), e);
		});
	</script>
@stop