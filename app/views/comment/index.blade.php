@extends('layouts.private.master')

@section('title')
	{{ $title }} 
@stop

@section('content')
	
	{{ Misc::makeMessage(); }}

	{{ $table }}

	<div class="pagination">
		<ul class="pagination">
			{{ $links }}
		</ul>
	</div>
@stop


@section('script')
	{{ HTML::script('js/askconfirm.js'); }}
	<script type="text/javascript">
		$('.askConfirm').click(function (e){
			askConfirm('آیا از حذف این دسته بندی اطمینان دارید؟', $(this), e);
		});
	</script>
@stop