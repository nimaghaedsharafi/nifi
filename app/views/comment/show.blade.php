@extends('layouts.private.master')

@section('title')
	{{ $title }} 
@stop

@section('content')
	{{ Misc::makeMessage(); }}
	
	<div class="row">
		<div class="col-md-9 col-xs-12">
			<p>{{ $comment->comment_text }}</p>
		</div>
	</div>
	<h3>عملیات: </h3>
	@if ($comment->status == COMMENT_NOTCHECKED) 
		<a class="btn btn-success" href="{{ URL::route('comment.approve', [$comment->id]) }}">قبول</a>
		<a class="btn btn-warning" href="{{ URL::route('comment.ignore', [$comment->id]) }}">رد</a>
	@elseif ($comment->status == COMMENT_APPROVED)
		<a class="btn btn-warning" href="{{ URL::route('comment.ignore', [$comment->id]) }}">رد</a>
	@elseif ($comment->status == COMMENT_IGNORED)
		<a class="btn btn-success" href="{{ URL::route('comment.approve', [$comment->id]) }}">قبول</a>
	@endif
	<a class="btn btn-danger" href="{{ URL::route('comment.destroy', [$comment->id]) }}">حذف</a>
	@if ($comment->status == COMMENT_NOTCHECKED) 
	<a class="btn btn-info" href="{{ URL::route('comment.notcheck', [$comment->id]) }}">بازگشت</a>
	@else 
	<a class="btn btn-info" href="{{ URL::route('comment.index', [$comment->id]) }}">بازگشت</a>
	@endif
@stop	