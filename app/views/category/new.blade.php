@extends('layouts.private.master')

@section('title')
	{{ $title }} 
@stop

@section('content')
	{{ Form::open(array('route' => 'category.new')) }}	
	<div class="row">
		<div class="col-md-5 col-sm-7 form-group">
			<label for="name"><span class="req"></span>نام دسته بندی</label>
			<input type="text" class="form-control" name="name" placeholder="نام دسته بندی" value="{{ Input::old('name', '') }}">
			{{ Misc::formError($errors, 'name') }}
		</div>
	</div>
	<div class="row">
		<div class="col-md-5 col-sm-6 form-group">
			<label for="sort"><span class="req"></span>اولیوت</label>
			{{ Form::text('sort', '0',  array('class' => 'form-control')) }}
			{{ Misc::formError($errors, 'sort') }}
			<span class="help-block">هر چه کوچکتر بالاتر نمایش داده می شود</span>
		</div>
	</div>
	<input type="submit" value="ثبت" class="btn btn-success">
	{{ Form::close() }}
@stop