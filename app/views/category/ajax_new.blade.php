@extends('layouts.private.ajaxModal')

@section('title')
	{{ $title }} 
@stop

@section('formopen')
	{{ Form::open(array('route' => 'category.new')) }}
@stop

@section('content')
	<div class="row">
		<div class="col-md-5 col-sm-8 form-group">
			<label for="name"><span class="req"></span>نام دسته بندی</label>
			<input type="text" class="form-control" name="name" placeholder="نام دسته بندی" value="{{ Input::old('name', '') }}">
			{{ Misc::formError($errors, 'name') }}
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 col-sm-3 form-group">
			<label for="sort"><span class="req"></span>اولیوت</label>
			{{ Form::text('sort', '0',  array('class' => 'form-control')) }}
			{{ Misc::formError($errors, 'sort') }}
			<span class="help-block">هر چه کوچکتر بالاتر نمایش داده می شود</span>
		</div>
	</div>
@stop

@section('footer')
	<input type="submit" value="ثبت" class="btn btn-success">
@stop