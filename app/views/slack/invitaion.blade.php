@extends('layouts.public.master')

@section('title')
	{{{ $title }}}
@stop

@section('content')	
	<h1>{{{ $title }}}</h1>
		<div class="row">
		<div class="col-md-12">
				<div class="col-md-6">
					<div><p>گروه اسلک <span class="en">GeekTalks</span> مکانی است برای صحبت و گفتگو درباره مسائل مختلف مربوط به برنامه‌نویسی ، تکنولوژی ، ایده ها ، کمک به رفع مشکلاتی که هنگام توسعه نرم‌افزار به آنها برخورد می‌کنیم و بطور کلی هر موضوعی که به حوزه کامپیوتر بر می گردد.</p><br><div>این گروه در آدرس <a href="https://geektalks.slack.com" class="en">geektalks.slack.com</a> قرار دارد و علاوه بر آن از اپ‌های <a href="https://itunes.apple.com/app/slack-app/id618783545?ls=1&amp;mt=8">آیفون</a>، <a href="https://play.google.com/store/apps/details?id=com.Slack">اندروید</a>، <a href="https://geektalks.slack.com/ssb/download-win">ویندوز</a> و <a href="https://itunes.apple.com/app/slack/id803453959?ls=1&amp;mt=12">مک</a> آن نیز می‌توانید استفاده کنید.<br>&nbsp;</div></div>
				</div>
				<div class="col-md-6">
					<?php if(Session::has('msg')) { ?>
					<div class="alert {{ Session::get('stat') }}" id="alert" role="alert"><span id="alertText">{{ Session::get('msg') }}</span></div>
					<?php } else { ?>
					<div class="alert alert-danger hide" id="alert" role="alert"><span id="alertText"></span></div>
					<?php } ?>
					<div class="well">
						<form id="loginForm" method="POST" action="{{ URL::route('slackinv.new') }}" >
						{{ Form::token() }}
							<div class="form-group">
								<label for="email" class="control-label"><span class="req"></span>آدرس ایمیل</label>
								<input type="email" class="form-control" id="email" name="email" autocomplete="false" required="required" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('آدرس ایمیلت رو درست تایپ نکردی!')" placeholder="آدرس ایمیل خود را وارد کنید" value="{{ Input::old('email') }}">
								{{ Misc::formError($errors, 'email') }}
							</div>
							<div class="form-group">
								<label for="firstname" class="control-label"><span class="req"></span>نام</label>
								<input type="text" class="form-control" id="firstname" name="firstname" autocomplete="false" required="required" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('نمی تونیم تو گروه بی اسم صدات کنیم که!')" placeholder="نام خود را وارد کنید" value="{{ Input::old('firstname') }}">
								{{ Misc::formError($errors, 'firstname') }}

							</div>
							<div class="form-group">
								<label for="lastname" class="control-label">نام خانوادگی</label>
								<input type="text" class="form-control" id="lastname" name="lastname" placeholder="نام خانوادگی خود را وارد کنید" value="{{ Input::old('lastname') }}" >
							</div>
							<div class="form-group">
								<label for="passcode" class="control-label"><span class="req"></span>رمز دسترسی</label>
								<input type="text" class="form-control" id="passcode" name="passcode" placeholder="رمز دسترسی رو اینجا وارد کنید." required value="{{ Input::old('passcode') }}" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('بدون رمز دسترسی نمیشه عضو شد!')">
								{{ Misc::formError($errors, 'passcode') }}
								<span class="help-block">برای عضو شدن در گروه باید رمز دسترسی داشته باشی. اگه رمز دسترسی نداری به من <a href="mailto:me@nifi.ir">ایملیل بزن.</a></span>
							</div>
							<button type="submit" class="btn btn-success btn-block">ارسال دعوتنامه</button>
						</form>
					</div>
				</div>
		 </div>
		</div>
@stop