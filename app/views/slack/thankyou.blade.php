@extends('layouts.public.master')

@section('title')
	{{{ $title }}}
@stop

@section('content')	
	<h1>{{{ $title }}}</h1>
		<div class="row">
			<div class="col-md-12">
				<div><p>
					<h4>دعوتنامه گروه اسلک GeekTalks شما به ایمیل {{ Misc::makeMeBold($email) }} ارسال شد.</h4><br>
					لطفا صندوق ایمیل خود را چک کنید.
				</p></div>
			</div>
		</div>
@stop