@extends('layouts.public.master')

@section('title')
	{{ $title }}
@stop

@section('content')
	@foreach($stories as $story)
		@include('layouts.public.article.shortshow', $story)
	@endforeach
	<div id="pagination">
		{{ $stories->links() }}
	</div>
@stop