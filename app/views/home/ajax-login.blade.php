<div id="login-overlay" class="modal-dialog">
			<div class="modal-content">
					<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title" id="myModalLabel">ورود به باشگاه</h4>
					</div>
					<div class="modal-body">
							<div class="row">
									<div class="col-xs-6">
											<div class="well">
													<form id="loginForm" method="POST" action="{{ URL::route('login') }}" >
															<div class="form-group">
																	<label for="username" class="control-label">نام کاربری</label>
																	<input type="text" class="form-control" id="username" name="username" autocomplete="false" required="required" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('نام کاربریتان را فراموش کردید')" placeholder="نام کابری خود را وارد کنید">
															</div>
															<div class="form-group">
																	<label for="password" class="control-label">رمز عبور</label>
																	<input type="password" class="form-control" id="password" name="password" autocomplete="false" required="required" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('رمز عبورتان را فراموش کردید')" placeholder="رمز عبور خود را وارد کنید">
															</div>
															<div class="checkbox">
																	<label>
																			<input type="checkbox" name="remember" id="remember"> مرا به خاطر بسپار
																	</label>
																	<p class="help-block">(اگر از کامپیوتر شخصی استفاده می کنید.)</p>
															</div>
															<button type="submit" class="btn btn-success btn-block">ورود به حساب کاربری</button>
															<a href="{{ URL::route('forgotpass') }}" class="btn btn-default btn-block">بازیابی رمز عبور</a>
													</form>
											</div>
									</div>
							</div>
					</div>
			</div>
	</div>