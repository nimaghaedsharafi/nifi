@extends('layouts.public.master')

@section('title')
	خوش آمدید
@stop

@section('content')	
	<h1>ورود به {{{ SConfig::get('system.name') }}}</h1>
		<div class="row">
		<div class="col-md-9">
				<div class="col-md-9">
					<?php if(Session::has('msg')) { ?>
					<div class="alert {{ Session::get('stat') }}" id="alert" role="alert"><span id="alertText">{{ Session::get('msg') }}</span></div>
					<?php } else { ?>
					<div class="alert alert-danger hide" id="alert" role="alert"><span id="alertText"></span></div>
					<?php } ?>
				</div>
				<div class="col-md-5">
					<div class="well">
						<form id="loginForm" method="POST" action="{{ URL::route('login') }}" >
						{{ Form::token() }}
							<div class="form-group">
								<label for="username" class="control-label">نام کاربری</label>
								<input type="text" class="form-control" id="username" name="username" autocomplete="false" required="required" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('نام کاربریتان را فراموش کردید')" placeholder="نام کابری خود را وارد کنید">
							</div>
							<div class="form-group">
								<label for="password" class="control-label">رمز عبور</label>
								<input type="password" class="form-control" id="password" name="password" autocomplete="false" required="required" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('رمز عبورتان را فراموش کردید')" placeholder="رمز عبور خود را وارد کنید">
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember" id="remember"> مرا به خاطر بسپار
								</label>
								<p class="help-block">(اگر از کامپیوتر شخصی استفاده می کنید.)</p>
							</div>
							<button type="submit" class="btn btn-success btn-block">ورود به حساب کاربری</button>
							<a href="{{ URL::route('forgotpass') }}" class="btn btn-default btn-block">بازیابی رمز عبور</a>
						</form>
					</div>
				</div>
		 </div>
		</div>
		<script type="text/javascript">
			function showAlert(txt) {
				$('#alertText').html(txt);
				$('#alert').addClass('alert-danger').removeClass('alert-info').removeClass('hide');
			}
		</script>
@stop