<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder {

	public function run()
	{
		$user = new User();

		$user->firstname = "نیما";
		$user->lastname = "قائدشرفی";
		$user->access = ADMIN_ACCESS;
		$user->email = "me@nifi.ir";
		$user->username = "nghaedsharafi";
		$user->password = "581321";

		$user->save();
	}

}