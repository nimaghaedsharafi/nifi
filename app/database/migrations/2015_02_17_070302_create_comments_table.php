<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('website');
			$table->text('comment_text');
			$table->unsignedInteger('parent_id')->nullable()->default(null);
			$table->foreign('parent_id')->references('id')->on('comments')->onDelete('cascade');
			$table->unsignedInteger('story_id')->unsigned();
			$table->foreign('story_id')->references('id')->on('stories')->onDelete('cascade');
			$table->integer('status')->default(COMMENT_NOTCHECKED);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
