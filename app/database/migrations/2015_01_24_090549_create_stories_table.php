<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('shorttext');
			$table->longText('fulltext');
			$table->integer('view');
			$table->unsignedInteger('author');
			$table->foreign('author')->references('id')->on('users')->onDelete('cascade');
			$table->boolean('publish');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stories');
	}

}
