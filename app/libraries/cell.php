<?php 

class Cell
{
	public $name;
	public $func = '';
	public $param = null;

	function __construct($name, $func = '', $param = null) {
		$this->name = $name;
		$this->func = $func;
		$this->param = $param;
	}
}