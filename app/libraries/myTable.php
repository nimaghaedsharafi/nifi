<?php 
/**
* Table Generator For Ease Of User in Ajax Request!
* Designed For Laravel 4
* Require Header.php and Cell.php
*/
class MyTable
{
	protected $headers = array();
	protected $cells = array();
	protected $tableClass = 'table table-hover';
	protected $tableID = '';
	protected $headClass = '';
	protected $rowClass = '';
	protected $cellClass = '';
	protected $rowCondAndCls = array();
	protected $operationClass = '';
	protected $headCellClass = '';
	protected $headCellWidth = '';
	protected $dirUp = 'dir-up';
	protected $dirDown = 'dir-down';
	protected $route = '';
	protected $routeParam = array();
	protected $onEmpty = '';
	protected $data = null;
	protected $dir = 'desc';
	protected $orderby = '';
	protected $operation = null;
	protected $openTagOnEmpty = '<div class="error"><h4>';
	protected $closeTagOnEmpty = '</h4></div>';
	protected $operationName = 'عملیات';
	protected $counter = 1;

	function __construct(array $initialize = array()) {
		$this->initialize($initialize);
	}
	
	private function initialize(array $initialize)
	{
		$classVar = get_class_vars(get_class($this));
		foreach ($initialize as $key => $value)
		{
			$this->$key = $value;
		}
	}

	private function tableOpenTag()
	{
		$string = '<table';
		
		if ($this->tableClass != '') {

			$string .= ' class="'. $this->tableClass . '" ';
		}
		if ($this->tableID != '') {
			
			$string .= ' id="'. $this->tableID . '" ';
		}
		
		$string .= '>';
		
		return $string;
	}

	private function tableCloseTag()
	{
		return '</table>';
	}

	private function headOpenTag()
	{
		$string = '<thead ';
		
		if ($this->headClass != '')
			$string .= 'class="'. $this->headClass . '"';
		
		$string .= ' >';

		return $string;
	}
	private function headCell($header, $key)
	{
		$string = '<td';
		
		if ($header == 'operation' and $this->operationClass != '') {

			$string .= ' class="'. $this->operationClass . '" ';
		}
		else if ($this->headCellClass != '') {

			$string .= ' class="'. $this->headCellClass . '" ';
		}

		if ($this->headCellWidth != '') {
			$string .= ' width="'. $this->headCellWidth . '" ';
		}
		
		$string .= '>';
		if ( $key->enabled )
		{
			$direction = '';
			if ($this->orderby == $header)
			{
				if ($this->dir == 'asc')
					$direction = 'desc';
				else
					$direction = 'asc';

			}
			$string .= '<a href="' . URL::route($this->route, array_merge($this->routeParam, array($header, $direction))). '"';
			if ($this->orderby == $header)
			{
				if ($this->dir == 'asc')	
					$string .= ' class= "' . $this->dirUp . '"';
				if ($this->dir == 'desc')
					$string .= ' class= "' . $this->dirDown . '"';
			}
			$string .= '>';
			$string .= $key->title;
			$string .= '</a>';
		}
		else
		{
			$string .= $key->title;
		}
		$string .= '</td>';

		return $string;
	}
	private function rowOpenTag($row)
	{
		$string = '<tr ';
		$cond = $this->rowClass != '';
		if ($cond) {
			$string .= 'class="'. $this->rowClass;
		}

		if(count($this->rowCondAndCls) != 0) {

			if(!$cond) {
				$string .= 'class="';
			}

			foreach($this->rowCondAndCls as $rc) {
				if($this->getExactValue($row, $rc['cond'])) {
					$string .= " " . $rc['cls'];
				}
			}
			$string .= '"';

		} else if($cond) {

			$string .= '"';
		}
		$string .= ' >';

		return $string;
	}

	private function cellOpenTag()
	{
		$string = '<td ';
		if ($this->cellClass != '')
			$string .= 'class="'. $this->cellClass . '"';
		$string .= ' >';

		return $string;
	}
	private function rowCloseTag()
	{
		return '</tr>';
	}
	private function cellCloseTag()
	{
		return '</td>';
	}
	private function headCloseTag()
	{
		return '</thead>';
	}
	private function makeHeaders()
	{
		$string = '';
	
		$string .= $this->headOpenTag();
		
		foreach($this->headers as $header => $key)
			$string .= $this->headCell($header, $key);
		
		$string .= $this->headCloseTag();

		return $string;
	}
	private function eachRow($row)
	{
		$string = '';

		foreach($this->cells as $cell)
		{
			$string .= $this->cellOpenTag();
			$string .= $this->makeCell($row, $cell);
			$string .= $this->cellCloseTag();
		}
		return $string;
	}	
	private function makeCell($row, $cell)
	{
		if($cell->name == 'counter') {
			return $this->counter++;
		}
		if ($cell->name == 'operation')
		{	
			$operationHTML = $this->makeOperationButton($row);
			$param = array();
			foreach ($this->operation as $item)
			{
				$value;
				if (!is_null($item->param))
				{
					$value = eval("?><?php return " . $item->param . " ?> <?php ");

				}
				if($item->route != '') {
					$param[] = URL::route($item->route, $value);
				}
			}
			return vsprintf($operationHTML, $param);
		}
		if ($cell->func != '')
		{
			return call_user_func_array($cell->func, array($this->getExactValue($row, $cell->name), $cell->param));
		}

		return $this->getExactValue($row, $cell->name);
	}
	private function getExactValue($row, $name)
	{
		$tempRow = clone $row;
		foreach( explode('->', $name) as $key => $value)
		{
			$rtrim = rtrim($value, '()');
			if ( $rtrim != $value )
			{
				$temp = $tempRow->{$rtrim}();
			}
			else
			{
				try {
					if(is_null($tempRow)) {
						return ;
					}
					$temp = $tempRow->{$value};
				} catch (Exception $e) {
					throw new Exception("Error Processing Request: " . $name , 1);
				}
			}
			unset($tempRow);
			$tempRow = $temp;
		}
		return $tempRow;
	}
	public function makeRows()
	{
		$string = '';

		if (count($this->data) == 0)
			return $this->openTagOnEmpty . $this->onEmpty . $this->closeTagOnEmpty;
		
		foreach($this->data as $row)
		{
			$string .= $this->rowOpenTag($row);
			$string .= $this->eachRow($row);
			$string .= $this->rowCloseTag();
		}
		return $string;
	}
	public function render()
	{
		if (count($this->data) == 0)
			return $this->openTagOnEmpty . $this->onEmpty . $this->closeTagOnEmpty;
		return $this->tableOpenTag() . $this->makeHeaders() . $this->makeRows() . $this->tableCloseTag();
	}

	public function makeOperationButton($row) {
		$button = '
        <div class="btn-group">
            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">' . $this->operationName . ' ' . '
                <span class="fa fa-caret-down"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
            ';
		foreach($this->operation as $item) {
			$button .= "<li>" . $item->btn($row) . "</li>";
		}

        $button .= '
            </ul>
        </div>
        ';
        return $button;
	}
}