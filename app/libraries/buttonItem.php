<?php 

/**
* Item of Operation Button 
* Built for Laravel 4 and Bootrstrap 3
* TableGenerator
*/	
class ButtonItem
{
	public $txt;
	public $icon;
	public $anchorClass;
	public $route;
	public $param;
	public $data;

	function __construct($txt, $icon = '', $anchorClass = '', $route = '', $param = '', $data = '')
	{
		$this->txt = $txt;
		$this->icon = $icon;
		$this->anchorClass = $anchorClass;
		$this->route = $route;
		$this->param = $param;
		$this->data = $data;
	}

	public function btn($row) {
		// Empty String
		$string = '';

		// some tiny comparison saving
		$needLink = strcmp($this->route, '') != 0;
		$needIcon = strcmp($this->icon, '') != 0;
		$needanchorClass = strcmp($this->anchorClass, '') != 0;
		$needData = is_array($this->data);

		// open anchor tag
		if($needLink) {
			$string .= "<a href='%s'"; 
			if($needanchorClass) {
				$string .= ' class="' . $this->anchorClass . '"';
			}
			if($needData) {	
				foreach ($this->data as $key => $value) {
					$string .= ' data-' . $key . '="' . $value . '"';
				}
			}
			$string .= ' title="' . $this->txt . '" >';
		}

		if($needIcon) {
			$string .= '<i class="' . $this->icon . '"></i> ';
		}

		$string .= $this->txt;

		// close anchor tag
		if($needLink) {
			$string .= "</a>";
		}

		return $string;
	}
    public function __toString() {
		
		// Empty String
		$string = '';

		// some tiny comparison saving
		$needLink = strcmp($route, '') != 0;
		$needIcon = strcmp($icon, '') != 0;

		// open anchor tag
		if($needLink) {
			$string .= "<a href='%s' title='" . $txt . "' >";
		}

		if($needIcon) {
			$string .= '<i class="' . $icon . '"></i> ';
		}

		$string .= $txt;

		// close anchor tag
		if($needLink) {
			$string .= "</a>";
		}

		return $string;
	}
}