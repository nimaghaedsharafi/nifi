<?php 

/**
* Item of Operation Button 
* Built for Laravel 4 and Bootrstrap 3
* TableGenerator
*/	
class MultiButtonItem extends ButtonItem
{
	public $cond;
	public $btnItem1;
	public $btnItem2;

	function __construct($cond, $btnItem1, $btnItem2)
	{
		$this->cond = $cond;
		$this->btnItem1 = $btnItem1;
		$this->btnItem2 = $btnItem2;
	}

	public function btn($row) {

		// get the cond result
		$this->getCondResult($row);

		return parent::btn($row);
	}
	private function getCondResult($row) {
		
		// get cond result
		$value = eval("?><?php return " . $this->cond . " ?> <?php ");

		// set the item to retreive the result
		$item = $this->btnItem1;
		if(!$value) {
			$item = $this->btnItem2;
		}

		// now set the attributes to use parent method
		$this->txt = $item->txt;
		$this->icon = $item->icon;
		$this->anchorClass = $item->anchorClass;
		$this->route = $item->route;
		$this->param = $item->param;
	}
}