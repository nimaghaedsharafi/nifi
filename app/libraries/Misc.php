<?php 
use Carbon\Carbon;

class Misc
{
	public static function niceUserName($user) {
		return $user->lastname . ', ' . $user->firstname;
	}

	public static function monthSelectList() {
		return [
			'1' => 'فروردین',
			'2' => 'اردیبهشت',
			'3' => 'خرداد',
			'4' => 'تیر',
			'5' => 'امرداد',
			'6' => 'شهریور',
			'7' => 'مهر',
			'8' => 'آبان',
			'9' => 'آذر',
			'10' => 'دی',
			'11' => 'بهمن',
			'12' => 'اسفند'
		];
	}
	public static function errorTag($msg) {
	
		return '<span class="frmerror text-danger" >' . $msg . '</span>';
	}
	public static function formError($errors, $name){
		
		if ($errors->has($name)) {
			return Misc::errorTag( $errors->first($name) );
		}
		return ;
	}
	public static function allFormErrors($errors) {
		if($errors->count() == 0) {
			return ;
		}
		$str = '<div class="alert alert-danger"><ul class="list list-unstyled">';
		foreach ($errors->all() as $error) {
			$str .= '<li>' . $error . '</li>';
		}
		$str .= '</div>';

		return $str;
	}
	public static function makeListMessages() {
		$string = '';
		
		if (Session::has('listmsg')) {

			$list = Session::get('listmsg');
			foreach($list as $item) {
				
				$string .= '<div class="alert ' .
						$item['stat'] .
						'" id="alert" role="alert"><span id="alertText">' .
						$item['msg'] . '</span></div>';
			}
		}
		return $string;
	}
	public static function makeMessage() {
		if (Session::has('msg')) {
			return '<div class="alert ' .
					Session::get('stat') .
					'" id="alert" role="alert"><span id="alertText">' .
					Session::get('msg') . '</span></div>';
		}
		return ;
	}
	public static function makeSelect($array, $key, $value, $secValue = '', $seprator = ' ')
	{
		$data = [];
		foreach($array as $row)
		{
			$data[ $row->$key ] = $row->$value . (($secValue != '') ? $seprator . $row->$secValue : '');
		}
		return $data;
	}
	public static function makeBoolIcon($bool) {
		return intval($bool);
	}

	public static function niceDateForge($date, $format = 'j F y - H:i')
	{
		return jDate::forge($date)->format($format);
	}

	public static function niceDate($date) {
		if ($date->getTimestamp() >= strtotime("today")) {
			return 'امروز';
		}
		if($date->getTimestamp() >= strtotime("yesterday")) {
			return 'دیروز';
		}

		return self::niceDateForge($date, 'y/m/j');
		
	}

	public static function formatBytes($size, $precision = 2)
	{ 
		$units = array('B', 'KB', 'MB', 'GB', 'TB');

		$bytes = $size;
		$bytes = max($bytes, 0); 
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
		$pow = min($pow, count($units) - 1); 
		$bytes /= (1 << (10 * $pow)); 

		return round($bytes, $precision) . $units[$pow];
	}
	public static function encodeID($id)
	{
		return base_convert($id, 10, 35);
	}
	public static function decodeID($string)
	{
		return base_convert($string, 35, 10);
	}
	public static function makeLink($title, $route, $param) {
		return HTML::link(URL::route($route, $param), $title);
	}
	public static function makeTimestampFromShamsiDate($date, $time = array(0, 0, 0)) {
		// extract day , month and year value from date
		list($year, $month, $day) = explode('/', $date);
		
		// make timestamp for held date form jalali to goregian
		$newDate = new jDateTime();
		$newDate = $newDate->mktime($time[0], $time[1], $time[2], $month, $day, $year);

		// now convert Timestamp to DateTime format for mysql limitation
		$dt = new \DateTime();
		return $dt->setTimestamp($newDate);
	}
	public static function makeBooleanSign($bool) {
		if ($bool)
			return '<i class="active-sign fa fa-check-circle fa-2x"> </i>';
		return '<i class="deactive-sign fa fa-times-circle fa-2x"> </i>';
	}
	public static function threeStateIcon($val, $arrcheck) {
		if ($val == $arrcheck['green'])
			return '<i class="green-sign fa fa-check-circle fa-2x"> </i>';
		else if ($val == $arrcheck['blue'])
			return '<i class="blue-sign fa fa-info-circle fa-2x"> </i>';
		else if ($val == $arrcheck['red'])
			return '<i class="red-sign fa fa-times-circle fa-2x"> </i>';
		return ;
	}

	public static function makeLinks2Anchor($text) {
		
    	$text = preg_replace('%(((f|ht){1}tp://)[-a-zA-^Z0-9@:\%_\+.~#?&//=]+)%i', '<a href="\\1">\\1</a>', $text);
    	$text = preg_replace('%([[:space:]()[{}])(www.[-a-zA-Z0-9@:\%_\+.~#?&//=]+)%i', '\\1<a target="_self" href="http://\\2">\\2</a>', $text);

        return $text;
	}
	public static function makeLinks2AnchorBlank($text) {
			
    	$text = preg_replace('%(((f|ht){1}tp://)[-a-zA-^Z0-9@:\%_\+.~#?&//=]+)%i', '<a href="\\1">\\1</a>', $text);
    	$text = preg_replace('%([[:space:]()[{}])(www.[-a-zA-Z0-9@:\%_\+.~#?&//=]+)%i', '\\1<a target="_blank" href="http://\\2">\\2</a>', $text);

        return $text;
	}
	// true => ascii
	// flase => non-ascii
	public static function textDirection($str) {
		if (strlen($str) == 0) 
			return false;
		return preg_match('/[\x00-\x80]+/', $str[0]);
	}
	public static function makeMeBold($str) {
		return '<strong>' . $str . '</strong>';
	}
	public static function makeMeSmall($str) {
		return '<small>' . $str . '</small>';
	}
	public static function wrong() {
		return self::makeBooleanSign(false);
	}
}