<?php 

class Header
{

	public $title; # header title
	public $enabled; # true : have active link , false: have any link
	
	function __construct($title, $enabled = true)
	{
		$this->title = $title;
		$this->enabled = $enabled;
	}
}