<?php

class Comment extends \Eloquent {
	

	public function story() {
		return $this->belongsTo('Story', 'story_id');
	}
	public function parent() {
		if(!is_null($this->parent_id)) {
			return $this->belongsTo('Comment', 'parent_id');
		}
	}
	public function scopeApproved($query) {
		return $query->where('status', '=', COMMENT_APPROVED);
	}
	public function scopeNotChecked($query) {
		return $query->where('status', '=', COMMENT_NOTCHECKED);
	}

	public function getGravatarURL() {
		return "http://www.gravatar.com/avatar/" . md5($this->email);
	}

	public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }
}