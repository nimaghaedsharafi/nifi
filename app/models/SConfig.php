<?php

class SConfig extends \Eloquent {
	
	protected $table = "config";
	protected $fillable = [];

	public $timestamps = false;

	public static function get($key) {
		
		return self::rememberForever()->where("ckey", '=', $key)->first()->cval;
	}
}