<?php

class Story extends \Eloquent {
	protected $fillable = array('title','shorttext','fulltext','publish');
	

	public function category() {
		return $this->belongsTo('Category', 'catid');
	}
	public function author() {
		return $this->belongsTo('User', 'author');
	}

	public function scopePublished($query) {
		return $query->where('publish', '=', STORY_PUBLISHED);
	}

	public function comment() {
		return $this->hasMany('Comment', 'story_id');
	}

}