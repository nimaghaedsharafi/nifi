<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class User extends Eloquent implements UserInterface {

	use UserTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	public function getName() {
		return $this->firstname . ' ' . $this->lastname;
	}
	public function setPasswordAttribute($value) {
		$this->attributes['password'] = Hash::make($value);
	}

	public function story() {
		return $this->hasMany('Story', 'author');
	}
}
