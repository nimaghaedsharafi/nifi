<?php

class Category extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;

	public function story() {
		return $this->hasMany('Story', 'catid');
	}
}