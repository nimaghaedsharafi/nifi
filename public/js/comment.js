jQuery.fn.highlight=function(c){function e(b,c){var d=0;if(3==b.nodeType){var a=b.data.toUpperCase().indexOf(c),a=a-(b.data.substr(0,a).toUpperCase().length-b.data.substr(0,a).length);if(0<=a){d=document.createElement("span");d.className="highlight";a=b.splitText(a);a.splitText(c.length);var f=a.cloneNode(!0);d.appendChild(f);a.parentNode.replaceChild(d,a);d=1}}else if(1==b.nodeType&&b.childNodes&&!/(script|style)/i.test(b.tagName))for(a=0;a<b.childNodes.length;++a)a+=e(b.childNodes[a],c);return d} return this.length&&c&&c.length?this.each(function(){e(this,c.toUpperCase())}):this};jQuery.fn.removeHighlight=function(){return this.find("span.highlight").each(function(){this.parentNode.firstChild.nodeName;with(this.parentNode)replaceChild(this.firstChild,this),normalize()}).end()};
var clicked = false;
$('#leavecomment').click(function() {
	if(clicked) {
		var result = validation();
		console.log(result);
		if(!result.status) {
			coderr = result.error;
			errstr = '';

			if(coderr == 'nameempty')  {
				errstr = "فراموش کردی اسمت رو به من بگی!";
			} else if (coderr == 'namelen') {
				errstr = "اسمت خیلی کوتاه هست!";
			} else if (coderr == 'emailempty') {
				errstr = "فراموش کردی ایمیلت رو به من بگی!";
			} else if (coderr == 'emailmatch') {
				errstr = "فکر کنم تو وارد کردن ایمیل اشتباه کردی!";
			} else if (coderr == 'msgempty') {
				errstr = "نمی خواهی دیدگاهت رو برام بنویسی؟!";
			} else if (coderr == 'msglong') {
				errstr = "ممنون که انقد وقت گذاشتی و دیدگاه نوشتی ولی خیلی طولانی هست.";
			}

			errstr = '<div class="alert alert-danger">' + errstr + '</div>';

			$('div#error').html(errstr);

			return false;
		}		
		return true;
	}
	$('#authComment').slideDown();
	clicked = true;
	return false;
});

function validation () {
	var email = $('#email').val().trim();
	var name = $('#name').val().trim();
	var msg = $('#msgt').val().trim();

	if( name == '' ) {
		return { 'status': false, 'error': 'nameempty' };
	} else if (name.length < 4) {		
		return { 'status': false, 'error': 'namelen' };
	} else if(email == '') {
		return { 'status': false, 'error': 'emailempty' };
	} else if(! /(.+)@(.+){2,}\.(.+){2,}/.test(email) ) {
		return { 'status': false, 'error': 'emailmatch' };
	} else if (msg == '') {
		return { 'status': false, 'error': 'msgempty' };
	} else if (msg > 5000) {
		return { 'status': false, 'error': 'msglong' };
	} 

	return { 'status': true };
}