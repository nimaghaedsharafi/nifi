function centerize(box) {

	w = $(window).width() / 2 - box.outerWidth() / 2,
	h = $(window).height() / 2 - box.outerHeight() / 2;

	box.css({
		'left': w,
		'top': h,
		'position': 'absolute',
		'duration': 100,
	});
};
jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}