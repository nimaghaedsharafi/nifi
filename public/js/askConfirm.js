function askConfirm(msg, link, event) {
	
	event.preventDefault();
	bootbox.confirm(msg, function(result){
		if(result) {
			window.location.href = link.attr('href');
		}
	});
}